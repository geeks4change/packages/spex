# SPEX - SuperProductivity EXtractor

Helps with time logs from [SuperProductivity](https://super-productivity.com/).

***This has some hard-coded assumptions, like SuperProductivity running as Snap on Ubuntu.***

## 1a) (Preferred) Install globally and safe via bin-plugin


```bash
# (If not already done) Add global bin dir for ubuntu to your $PATH
>>~/.profile echo -e "\nPATH=$(composer global config bin-dir --absolute --quiet):\$PATH"
source ~/.profile
# (If not already done) Require the bin plugin globally.
composer global require bamarni/composer-bin-plugin
# For package separation, this should be the only thing required outside a bin.
# Require spex in its bin.
composer global bin spex require geeks4change/spex
```

## 1b) Install in a project dir

Do `composer require geeks4change/spex`, then run `bin/spex` / `bin/spex-sync`

## 2) Query

```bash
# Show help
spex
# Query by User, Week, Project, Day, Task.
spex uwpdt
# Filter by user, week, project-prefix.
spex t -u merlin -w+0-3 -p g4c
# Query synced data.
spex ut -f
# Show debug data
spex t -v
```

## 3a) Set up sync from existing repo

```bash
mkdir -p ~/.local/share/spex
cd ~/.local/share/spex
git clone git@remote:my/repo data
touch "data/$USER.json.age""
# If you have another repo, too:
# git clone git@remote:other/repo other
```

## 3b) Set up sync from scratch

```bash
# If you do not have a ssh key, do `ssh-keygen -t rsa`
# You can add other dirs besides "data".
mkdir -p ~/.local/share/spex/data
cd -p ~/.local/share/spex/data
## Change as needed
echo "project-prefix1" >project-prefixes.txt
echo "project-prefix2" >>project-prefixes.txt
cat ~/.ssh/id_rsa.pub >authorized_keys.txt
cat ~/some/other/id_rsa.pub >>authorized_keys.txt
touch "$USER.json.age"
git init && git add . && git commit -m Init
git remote add origin git@remote:my/repo
```

## 4) Prepare sync by installing 'rage'

Install [rage](https://github.com/str4d/rage) encryption tool from [release](https://github.com/str4d/rage/releases):
```bash
# Go to releases and use latest!
wget https://github.com/str4d/rage/releases/download/v0.5.1/rage_0.5.1_amd64.deb
sudo apt install rage_0.5.1_amd64.deb
```

## 5) Do the sync

```bash
# This will pull, sync, commit, and push.
spex-sync
```

