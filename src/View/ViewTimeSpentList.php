<?php

namespace geeks4change\spex\View;

use Symfony\Component\Console\Output\OutputInterface;

class ViewTimeSpentList implements ViewItemInterface {

  /**
   * @var \geeks4change\spex\View\ViewTimeSpent
   */
  protected $timeSpentItems = [];

  /**
   * @var string
   */
  protected $label;

  /**
   * ViewTimeSpentList constructor.
   * @param string $label
   */
  public function __construct(string $label) {
    $this->label = $label;
  }

  public function getLabel(): string {
    return $this->label;
  }

  public function addTimeSpent(ViewTimeSpent $timeSpent): void {
    $key = $timeSpent->getTaskId() . ':' . $timeSpent->getDay();
    if (isset($this->timeSpentItems[$key])) {
      throw new \LogicException("Duplicate key: $key");
    }
    $this->timeSpentItems[$key] = $timeSpent;
  }

  public function sort(): void {
    // Nothing to do here, as items will never be printed.
    // But for debugging...
    ksort($this->timeSpentItems);
  }

  public function write(OutputInterface $output): void {
    // Nothing to do here, as items will never be printed.
  }

  public function getTimeSum(): int {
    return array_reduce($this->timeSpentItems, function ($sum, ViewTimeSpent $spent) {
      return $sum + $spent->getTimeSpent();
    }, 0);
  }

}
