<?php

namespace geeks4change\spex\View;

use geeks4change\spex\View\Field\FieldInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class View implements ViewItemInterface {

  /**
   * @var \geeks4change\spex\View\Field\FieldInterface
   */
  protected $field;

  /**
   * @var \geeks4change\spex\View\Field\FieldInterface[]
   */
  protected $fields;

  /**
   * @var \geeks4change\spex\View\ViewItemInterface[]
   */
  protected $items = [];

  /**
   * @var string
   */
  protected $label;

  public function __construct(string $label, FieldInterface $field, FieldInterface ...$fields) {
    $this->label = $label;
    $this->field = $field;
    $this->fields = $fields;
  }

  /**
   * @param \geeks4change\spex\View\Field\FieldInterface ...$fields
   */
  public static function create(string $label, FieldInterface ...$fields) {
    if ($fields) {
      $field = array_shift($fields);
      return new self($label, $field, ...$fields);
    }
    else {
      return new ViewTimeSpentList($label);
    }
  }

  public function addTimeSpent(ViewTimeSpent $timeSpent): void {
    $key = $this->field->makeKey($timeSpent);
    $item =& $this->items[$key];
    if (!isset($item)) {
      $item = self::create($this->field->makeLabel($timeSpent), ...$this->fields);
    }
    $item->addTimeSpent($timeSpent);
  }

  public function sort(): void {
    ksort($this->items);
    foreach ($this->items as $item) {
      $item->sort();
    }
  }

  public function write(OutputInterface $output): void {
    $pattern = $this->field->makePattern();
    foreach ($this->items as $key => $item) {
      $time = $this->printTime($item->getTimeSum());
      if ($pattern) {
        $output->writeln(strtr($pattern, ['{time}' => $time, '{value}' => $item->getLabel()]));
      }
      $item->write($output);
    }
  }

  public function getTimeSum(): int {
    return array_reduce($this->items, function (int $sum, ViewItemInterface $item) {
      return $sum + $item->getTimeSum();
    }, 0);
  }

  public function getLabel(): string {
    return $this->label;
  }

  private function printTime(int $seconds) {
    $hours = floor($seconds / 3600);
    $minutes = floor(($seconds / 60) % 60);
    return sprintf('%d:%02d', $hours, $minutes);
  }

}
