<?php

namespace geeks4change\spex\View;

class ViewTimeSpent {

  /**
   * @var ?string
   */
  protected $projectName;

  /**
   * @var ?string
   */
  protected $taskName;

  /**
   * @var ?string
   */
  protected $taskId;

  /**
   * @var ?string
   */
  protected $user;

  /**
   * @var ?string
   */
  protected $day;

  /**
   * @var ?int
   */
  protected $timeSpent;

  public function __construct(?string $projectName = NULL, ?string $taskName = NULL, ?string $taskId = NULL, ?string $user = NULL, ?string $day = NULL, ?int $timeSpent = NULL) {
    $this->projectName = $projectName;
    $this->taskName = $taskName;
    $this->taskId = $taskId;
    $this->user = $user;
    $this->day = $day;
    $this->timeSpent = $timeSpent;
  }

  public function getProjectName(): string {
    return $this->projectName;
  }

  public function getTaskName(): string {
    return $this->taskName;
  }

  public function getTaskId(): string {
    return $this->taskId;
  }

  public function getUser(): string {
    return $this->user;
  }

  public function getDay(): string {
    return $this->day;
  }

  public function getTimeSpent(): int {
    return $this->timeSpent;
  }

}
