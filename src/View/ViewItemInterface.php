<?php

namespace geeks4change\spex\View;

use Symfony\Component\Console\Output\OutputInterface;

interface ViewItemInterface {

  public function addTimeSpent(ViewTimeSpent $timeSpent): void;
  public function sort(): void;
  public function write(OutputInterface $output): void;
  public function getTimeSum(): int;
  public function getLabel(): string;

}
