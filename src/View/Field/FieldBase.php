<?php

namespace geeks4change\spex\View\Field;

use geeks4change\spex\View\ViewTimeSpent;

abstract class FieldBase implements FieldInterface {

  /**
   * @var string
   */
  protected $pattern;

  /**
   * @var bool
   */
  protected $visible;

  public function __construct(string $pattern = '', bool $visible = TRUE) {
    $this->pattern = $pattern;
    $this->visible = $visible;
  }

  public function makePattern(): string {
    return $this->pattern;
  }

  public function makeKey(ViewTimeSpent $timeSpent): string {
    return strtolower($this->makeLabel($timeSpent));
  }

}
