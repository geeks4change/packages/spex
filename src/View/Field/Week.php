<?php

namespace geeks4change\spex\View\Field;

use geeks4change\spex\View\ViewTimeSpent;

class Week extends FieldBase {

  public function makeLabel(ViewTimeSpent $timeSpent): string {
    $day = $timeSpent->getDay();
    if ($locale = setlocale(LC_CTYPE, 0)) {
      setlocale(LC_ALL, $locale);
    }
    $date = new \DateTimeImmutable($day);
    $weekPrinted = strftime('%Y week %V', $date->getTimestamp());

    return $weekPrinted;
  }

}
