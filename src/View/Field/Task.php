<?php

namespace geeks4change\spex\View\Field;

use geeks4change\spex\View\ViewTimeSpent;

class Task extends FieldBase {

  public function makeLabel(ViewTimeSpent $timeSpent): string {
    return $timeSpent->getTaskName();
  }

}
