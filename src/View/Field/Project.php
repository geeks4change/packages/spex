<?php

namespace geeks4change\spex\View\Field;

use geeks4change\spex\View\ViewTimeSpent;

class Project extends FieldBase {

  public function makeLabel(ViewTimeSpent $timeSpent): string {
    $projectName = $timeSpent->getProjectName();
    return "[$projectName]";
  }

}
