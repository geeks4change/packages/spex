<?php

namespace geeks4change\spex\View\Field;

use geeks4change\spex\View\ViewTimeSpent;

interface FieldInterface {
  public function makePattern(): string;
  public function makeLabel(ViewTimeSpent $timeSpent): string;
  public function makeKey(ViewTimeSpent $timeSpent): string;
}