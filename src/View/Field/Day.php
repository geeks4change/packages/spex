<?php

namespace geeks4change\spex\View\Field;

use geeks4change\spex\View\ViewTimeSpent;

class Day extends FieldBase {

  public function makeLabel(ViewTimeSpent $timeSpent): string {
    $date = $this->makeDay($timeSpent);
    $dayPrinted = strftime('%Yw%V.%u: %a, %d.%b %Y', $date->getTimestamp());
    return $dayPrinted;
  }

  public function makeKey(ViewTimeSpent $timeSpent): string {
    $date = $this->makeDay($timeSpent);
    $dayPrinted = strftime('%Yw%V.%u', $date->getTimestamp());
    return $dayPrinted;
  }

  private function makeDay(ViewTimeSpent $timeSpent): \DateTimeImmutable {
    $day = $timeSpent->getDay();
    if ($locale = setlocale(LC_CTYPE, 0)) {
      setlocale(LC_ALL, $locale);
    }
    $date = new \DateTimeImmutable($day);
    return $date;
  }

}
