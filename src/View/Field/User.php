<?php

namespace geeks4change\spex\View\Field;

use geeks4change\spex\View\ViewTimeSpent;

class User extends FieldBase {

  public function makeLabel(ViewTimeSpent $timeSpent): string {
    return $timeSpent->getUser();
  }

}
