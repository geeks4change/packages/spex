<?php

namespace geeks4change\spex\View;

use geeks4change\spex\Filter\TimeSpentFilterInterface;
use geeks4change\spex\SuperProductivity\Backup;

class ViewBuilder {

  /**
   * @var \geeks4change\spex\View\View
   */
  protected $view;

  /**
   * @var \geeks4change\spex\Filter\TimeSpentFilterInterface|null
   */
  protected $filter;

  /**
   * @var TimeSpentFilterInterface|null
   */
  protected $timeSpentFilter;

  public function __construct(View $view, TimeSpentFilterInterface $filter) {
    $this->view = $view;
    $this->filter = $filter;
  }

  public function add(Backup $backup, string $name): void {
    $projectNames = [];
    foreach ($backup->getProjectList()->getProjects() as $sourceProject) {
      $projectName = $sourceProject->getTitle();
      $projectNames[$sourceProject->getId()] = $projectName;
    }

    $sourceTasks = $backup->getTaskList()->getTasks() +
      $backup->getArchivedTaskList()->getTasks();
    foreach ($sourceTasks as $sourceTask) {
      foreach ($sourceTask->getTimeSpentOnDay() as $day => $msec) {
        $projectName = $projectNames[$sourceTask->getProjectId()] ?? 'NONE';
        $timeSpent = new ViewTimeSpent($projectName, $sourceTask->getTitle(), $sourceTask->getId(), $name, $day, floor($msec / 1000));
        if ($this->filter->filterTimeSpent($timeSpent)) {
          $this->view->addTimeSpent($timeSpent);
        }
      }
    }
    $this->view->sort();
  }

}
