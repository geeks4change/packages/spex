<?php

namespace geeks4change\spex\SuperProductivity;

class Task {

  /**
   * @var string
   */
  protected $id;

  /**
   * @var string
   */
  protected $title;

  /**
   * @var string
   */
  protected $projectId;

  /**
   * @var array<string, integer>
   */
  protected $timeSpentOnDay;

  /**
   * Task constructor.
   * @param string $id
   * @param string $title
   * @param string|null $projectId
   * @param int[] $timeSpentOnDay
   */
  public function __construct(string $id, string $title, ?string $projectId, array $timeSpentOnDay) {
    $this->id = $id;
    $this->title = $title;
    $this->projectId = $projectId;
    $this->timeSpentOnDay = $timeSpentOnDay;
  }

  /**
   * @return string
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * @return string
   */
  public function getTitle(): string {
    return $this->title;
  }

  /**
   * @return ?string
   */
  public function getProjectId(): ?string {
    return $this->projectId;
  }

  /**
   * @return int[]
   */
  public function getTimeSpentOnDay(): array {
    return $this->timeSpentOnDay;
  }

}
