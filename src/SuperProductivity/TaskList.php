<?php

namespace geeks4change\spex\SuperProductivity;

use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Serializer\Normalizer\DenormalizableInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class TaskList implements DenormalizableInterface {

  /**
   * @SerializedName("entities")
   * @var \geeks4change\spex\SuperProductivity\Task[]
   */
  protected $tasks = [];

  /**
   * @return \geeks4change\spex\SuperProductivity\Task[]
   */
  public function getTasks(): array {
    return $this->tasks;
  }

  public function denormalize(DenormalizerInterface $denormalizer, $data, string $format = NULL, array $context = []) {
    // Trigger ArrayDenormalizer. Must not have constructor.
    if (isset($data['entities'])) {
      $this->tasks = $denormalizer->denormalize($data['entities'], Task::class . '[]');
    }
  }

  public function addTask(Task $task) {
    $this->tasks[$task->getId()] = $task;
  }

}
