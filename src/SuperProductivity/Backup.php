<?php

namespace geeks4change\spex\SuperProductivity;

use geeks4change\spex\Filter\ProjectFilterInterface;
use geeks4change\spex\Filter\TimeSpentFilterInterface;
use geeks4change\spex\View\ViewTimeSpent;
use Symfony\Component\Serializer\Annotation\SerializedName;

class Backup {

  /**
   * @SerializedName("project")
   * @var \geeks4change\spex\SuperProductivity\ProjectList
   */
  protected $projectList;

  /**
   * @SerializedName("task")
   * @var \geeks4change\spex\SuperProductivity\TaskList
   */
  protected $taskList;

  /**
   * @SerializedName("taskArchive")
   * @var \geeks4change\spex\SuperProductivity\TaskList
   */
  protected $archivedTaskList;

  /**
   * Backup constructor.
   * @param \geeks4change\spex\SuperProductivity\ProjectList $projectList
   * @param \geeks4change\spex\SuperProductivity\TaskList $taskList
   * @param \geeks4change\spex\SuperProductivity\TaskList $archivedTaskList
   */
  public function __construct(ProjectList $projectList, TaskList $taskList, TaskList $archivedTaskList) {
    $this->projectList = $projectList;
    $this->taskList = $taskList;
    $this->archivedTaskList = $archivedTaskList;
  }

  /**
   * @return \geeks4change\spex\SuperProductivity\ProjectList
   */
  public function getProjectList(): ProjectList {
    return $this->projectList;
  }

  /**
   * @return \geeks4change\spex\SuperProductivity\TaskList
   */
  public function getTaskList(): TaskList {
    return $this->taskList;
  }

  /**
   * @return \geeks4change\spex\SuperProductivity\TaskList
   */
  public function getArchivedTaskList(): TaskList {
    return $this->archivedTaskList;
  }

  public function duplicateFiltered(TimeSpentFilterInterface $filter) {
    $projectList = new ProjectList();
    $taskList = new TaskList();
    $archivedTaskList = new TaskList();
    foreach ($this->projectList->getProjects() as $project) {
      $projectName = $project ? $project->getTitle() : 'NONE';
      $fakeTimeSpent = new ViewTimeSpent($projectName);
      if ($filter->filterTimeSpent($fakeTimeSpent)) {
        $projectList->addProject($project);
      }
    }
    foreach ($this->taskList->getTasks() as $task) {
      $projectId = $task->getProjectId();
      $project = $projectList->getProjects()[$projectId] ?? NULL;
      $projectName = $project ? $project->getTitle() : 'NONE';
      $fakeTimeSpent = new ViewTimeSpent($projectName, $task->getTitle(), $task->getId());
      if ($filter->filterTimeSpent($fakeTimeSpent)) {
        $taskList->addTask($task);
      }
    }
    foreach ($this->archivedTaskList->getTasks() as $task) {
      $projectId = $task->getProjectId();
      $project = $projectList->getProjects()[$projectId] ?? NULL;
      $projectName = $project ? $project->getTitle() : 'NONE';
      $fakeTimeSpent = new ViewTimeSpent($projectName, $task->getTitle(), $task->getId());
      if ($filter->filterTimeSpent($fakeTimeSpent)) {
        $archivedTaskList->addTask($task);
      }
    }
    $duplicate = new static($projectList, $taskList, $archivedTaskList);
    return $duplicate;
  }

}
