<?php

namespace geeks4change\spex\SuperProductivity;

class Project {

  /**
   * @var string
   */
  protected $id;

  /**
   * @var string
   */
  protected $title;

  /**
   * @var string[]
   */
  protected $taskIds;

  /**
   * @var string[]
   */
  protected $backlogTaskIds;

  /**
   * Project constructor.
   * @param string $id
   * @param string $title
   * @param string[] $taskIds
   * @param string[] $backlogTaskIds
   */
  public function __construct(string $id, string $title, array $taskIds, array $backlogTaskIds) {
    $this->id = $id;
    $this->title = $title;
    $this->taskIds = $taskIds;
    $this->backlogTaskIds = $backlogTaskIds;
  }

  /**
   * @return string
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * @return string
   */
  public function getTitle(): string {
    return $this->title;
  }

  /**
   * @return string[]
   */
  public function getTaskIds(): array {
    return $this->taskIds;
  }

  /**
   * @return string[]
   */
  public function getBacklogTaskIds(): array {
    return $this->backlogTaskIds;
  }

}
