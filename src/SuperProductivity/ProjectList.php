<?php

namespace geeks4change\spex\SuperProductivity;

use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Serializer\Normalizer\DenormalizableInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class ProjectList implements DenormalizableInterface {

  /**
   * @SerializedName("entities")
   * @var \geeks4change\spex\SuperProductivity\Project[]
   */
  protected $projects = [];

  /**
   * @return \geeks4change\spex\SuperProductivity\Project[]
   */
  public function getProjects(): array {
    return $this->projects;
  }

  public function denormalize(DenormalizerInterface $denormalizer, $data, string $format = NULL, array $context = []) {
    // Trigger ArrayDenormalizer. Must not have constructor.
    if (isset($data['entities'])) {
      $this->projects = $denormalizer->denormalize($data['entities'], Project::class . '[]');
    }
  }

  public function addProject(Project $project) {
    $this->projects[$project->getId()] = $project;
  }

}

