<?php

namespace geeks4change\spex;

use Doctrine\Common\Annotations\AnnotationReader;
use geeks4change\spex\SuperProductivity\Backup;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\CustomNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class Reader {

  public function read(string $data): Backup {
    return $this->makeSerializer(new JsonEncoder())
      ->deserialize($data, Backup::class, 'json');
  }

  public function write(Backup $backup): string {
    return $this->makeSerializer(new JsonEncoder())
      ->serialize($backup, 'json');
  }

  public function makeSerializer($encoder): Serializer {
    $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
    $normalizers = [
      new CustomNormalizer(),
      new ObjectNormalizer(
        new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader())),
        new MetadataAwareNameConverter($classMetadataFactory),
        NULL,
        new PropertyInfoExtractor()
      ),
      new ArrayDenormalizer(),
    ];
    $serializer = new Serializer($normalizers, [$encoder]);
    return $serializer;
  }

}
