<?php

namespace geeks4change\spex\Commands;

use geeks4change\spex\View\Field\Day;
use geeks4change\spex\View\Field\User;
use geeks4change\spex\View\Field\Project;
use geeks4change\spex\View\Field\Task;
use geeks4change\spex\View\Field\Week;
use geeks4change\spex\View\View;
use geeks4change\spex\View\ViewBuilder;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ViewCommand extends CommandBase {

  protected static $defaultName = 'view';

  protected function configure() {
    parent::configure();
    $this
      ->addArgument('groupings', InputArgument::OPTIONAL, 'Add letters for grouping, like "uwpdt" for User, Week, Project, Day, Task.')
      ->addOption('week', 'w', InputOption::VALUE_OPTIONAL, 'The week range like 2030w23+3, w+0-4,  w42-1, w17.', FALSE)
      ->addOption('project', 'p', InputOption::VALUE_IS_ARRAY|InputOption::VALUE_OPTIONAL, 'The project prefix')
      ->addOption('task', 't', InputOption::VALUE_IS_ARRAY|InputOption::VALUE_OPTIONAL, 'The task prefix')
      ->addOption('user', 'u', InputOption::VALUE_REQUIRED, 'The user name to filter for.')
      ->addOption('file', 'f', InputOption::VALUE_OPTIONAL, 'The file or directory to read. If omitted, reads from last backup. If given but empty, read from sync dir.', FALSE)
      ->setDescription('Extract spent times. See https://gitlab.com/geeks4change/packages/spex')
    ;
  }

  protected function execute(InputInterface $input, OutputInterface $output) {
    $chars = $input->getArgument('groupings') ? str_split($input->getArgument('groupings')) : [];
    if (!$chars) {
      return $this->getApplication()->find('help')
        ->run(new ArrayInput(['command_name' => 'view']), $output);
    }
    if (count($chars) >= 2) {
      $prefixes = [];
      foreach (range(1, count($chars) - 1) as $level) {
        $prefixes[] = str_repeat("\n", count($chars) - 1 - $level)
          . str_repeat('#', $level) . ' ';
      }
    }
    if (count($chars) >= 1) {
      $prefixes[] = "- ";
    }
    $classes = [
      'w' => Week::class,
      'p' => Project::class,
      't' => Task::class,
      'd' => Day::class,
      'u' => User::class,
    ];

    $fields = [];
    $i = 0;
    foreach ($chars as $char) {
      $class = $classes[$char] ?? $this->throwInvalidChar($char);
      $fields[$i] = new $class($prefixes[$i] . '({time}) {value}');
      $i++;
    }
    $view = View::create('', ...$fields);

    $fileOrDir = $input->getOption('file') ?? "{$_SERVER['HOME']}/.local/share/spex/data";
    if ($fileOrDir === FALSE) {
      $fileOrDir = $this->lastBackupName($output);
    }
    if (is_file($fileOrDir) && is_readable($fileOrDir)) {
      $name = $input->getOption('user') ?? $_SERVER['USER'];
      $fileNames = [$name => $fileOrDir];
    }
    elseif (is_dir($fileOrDir)) {
      $fileNames = glob("$fileOrDir/*.json{,.age}", GLOB_BRACE);
      $names = array_map(function (string $fileName) {
        return pathinfo($fileName, PATHINFO_FILENAME);
      }, $fileNames);
      $fileNames = array_combine($names, $fileNames);
    }
    else {
      throw new \UnexpectedValueException("Invalid file or dir: $fileOrDir");
    }

    $viewBuilder = new ViewBuilder($view, $this->makeFilters($input));
    foreach ($fileNames as $name => $fileName) {
      $backup = $this->readBackup($fileName);
      $viewBuilder->add($backup, $name);
    }
    $view->write($output);
    return 0;
  }

  private function throwInvalidChar($char) {
    throw new \RuntimeException("Invalid grouping: $char.\nValid are: wptd");
  }

}
