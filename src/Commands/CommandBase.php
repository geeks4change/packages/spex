<?php

namespace geeks4change\spex\Commands;

use geeks4change\spex\Filter\CompoundFilter;
use geeks4change\spex\Filter\ProjectPrefixesFilter;
use geeks4change\spex\Filter\TaskPrefixesFilter;
use geeks4change\spex\Filter\TimeSpentFilterInterface;
use geeks4change\spex\Filter\UserFilter;
use geeks4change\spex\Filter\WeekFilter;
use geeks4change\spex\Reader;
use geeks4change\spex\SuperProductivity\Backup;
use geeks4change\spex\Utility\RageEncryption;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CommandBase extends Command {

  /**
   * @var \geeks4change\spex\Utility\RageEncryption
   */
  protected $encryption;

  public static function create() {
    $instance = new static();
    $instance->encryption = new RageEncryption();
    return $instance;
  }

  protected function configure() {
  }

  protected function makeFilters(InputInterface $input, array $excludeFilters = []): TimeSpentFilterInterface {
    $filters = new CompoundFilter();

    if ($input->hasOption('week') && !in_array('week', $excludeFilters)) {
      // We need this voodoo to handle empty (NULL) vs no option (FALSE).
      $weekSpec = $input->getOption('week') ?? '';
      $weekSpec = ($weekSpec === FALSE) ? NULL : $weekSpec;
      if (isset($weekSpec)) {
        $filters->addFilter(WeekFilter::fromSpec($weekSpec));
      }
    }

    if ($input->hasOption('project') && !in_array('project', $excludeFilters)) {
      if ($projectSpec = $input->getOption('project')) {
        $filters->addFilter(new ProjectPrefixesFilter($projectSpec));
      }
    }

    if ($input->hasOption('task') && !in_array('task', $excludeFilters)) {
      if ($projectSpec = $input->getOption('task')) {
        $filters->addFilter(new TaskPrefixesFilter($projectSpec));
      }
    }

    if ($input->hasOption('user') && !in_array('user', $excludeFilters)) {
      if ($userSpec = $input->getOption('user')) {
        $filters->addFilter(new UserFilter($userSpec));
      }
    }
    return $filters;
  }

  protected function lastBackupName(OutputInterface $output): string {
    $fileNames = array_merge(
      glob($_SERVER['HOME'] . '/snap/superproductivity/current/.config/superProductivity/backups/*'),
      // New location starting from 2021-11.
      glob($_SERVER['HOME'] . '/snap/superproductivity/common/.config/superProductivity/backups/*')
    );
    if ($fileNames) {
      $fileName = $fileNames[count($fileNames) - 1];
      if ($output->isVerbose()) {
        $output->writeln("Using backup file: $fileName");
      }
    }
    else {
      throw new \RuntimeException("Could not find backup.");
    }
    return $fileName;
  }

  protected function readBackup(string $fileName): Backup {
    if (substr($fileName, -9) === '.json.age') {
      $data = $this->encryption->read($fileName, $_SERVER['HOME'] . '/.ssh/id_rsa');
    }
    elseif (substr($fileName, -5) === '.json') {
      $data = file_get_contents($fileName);
    }
    else {
      throw new \UnexpectedValueException("Can not handle file $fileName");
    };
    $backup = (new Reader())->read($data);
    return $backup;
  }

}
