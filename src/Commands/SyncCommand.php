<?php

namespace geeks4change\spex\Commands;

use geeks4change\spex\Filter\ProjectPrefixesFilter;
use geeks4change\spex\Reader;
use geeks4change\spex\Utility\ShellExecutor;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SyncCommand extends CommandBase  {

  protected static $defaultName = 'sync';

  protected function configure() {
    parent::configure();
    $this
      ->addOption('project', 'p', InputOption::VALUE_IS_ARRAY|InputOption::VALUE_OPTIONAL, 'Only projects with this prefix(es) will be synced.')
      ->addOption('file', 'f', InputOption::VALUE_REQUIRED|InputOption::VALUE_IS_ARRAY, 'The file(s) to sync to, defaults to ~/.local/share/spex/*/{$user}.json.')
      ->addOption('user', 'u', InputOption::VALUE_REQUIRED, "The user name, defaults to contents of '~/.local/share/spex/user' if exists, otherwise current system user name: {$_SERVER['USER']}.")
      ->setDescription('Sync spent times filtered by project.')
    ;
  }

  protected function execute(InputInterface $input, OutputInterface $output) {
    $this->encryption->checkBinary();
    $user = $input->getOption('user')
      ?? $this->resdFromFile("{$_SERVER['HOME']}/.local/share/spex/user")
      ?? $_SERVER['USER'];

    $fileNames = $input->getOption('file')
      ?: glob("{$_SERVER['HOME']}/.local/share/spex/*/{$user}.json.age")
      ?: ["{$_SERVER['HOME']}/.local/share/spex/data/{$user}.json.age"];
    foreach ($fileNames as $fileName) {
      if (!$output->isQuiet()) {
        $output->writeln("Syncing to: $fileName");
      }
      $dirName = dirname($fileName);
      $quotedDirName = escapeshellarg($dirName);
      ShellExecutor::succeed("mkdir -p $quotedDirName");

      $projectPrefixesFile = "$dirName/project-prefixes.txt";
      $projectPrefixesContent = file_get_contents($projectPrefixesFile);
      if (!$projectPrefixesContent) {
        throw new \RuntimeException("Please create a file with project prefixes, one per line, at $projectPrefixesFile");
      }
      $projectPrefixes = preg_split('/(\r\n|\r|\n)/', $projectPrefixesContent);
      $filter = new ProjectPrefixesFilter($projectPrefixes);

      $backup = $this->readBackup($this->lastBackupName($output));
      $duplicateBackup = $backup->duplicateFiltered($filter);
      $serialized = (new Reader())->write($duplicateBackup);

      $recipientFiles = glob("$dirName/authorized_keys.txt");
      if (!$recipientFiles) {
        throw new \RuntimeException("You must first add some public keys (like in id_rsa.pub), one per line, to $dirName/authorized_keys.txt");
      }

      $hasGit = is_dir("$dirName/.git");
      if ($hasGit) {
        // Assure that we are on master.
        ShellExecutor::succeed("git -C $quotedDirName rev-parse --abbrev-ref HEAD", $revParseStdout);
        $revParseStdout = trim($revParseStdout);
        if ($revParseStdout !== 'master') {
          throw new \RuntimeException("We must be on branch master. Currently: '$revParseStdout'");
        }
        // Assure that working dir is clean.
        ShellExecutor::succeed("git -C $quotedDirName status --porcelain", $statusStdout);
        if ($statusStdout) {
          throw new \RuntimeException("Dir has uncommitted changes: $dirName");
        }
        // Check if we have a "origin" remote.
        $lsRemoteExitCode = ShellExecutor::exec("git -C $quotedDirName ls-remote --exit-code origin master");
        $hasRemote = $lsRemoteExitCode === 0 || $lsRemoteExitCode === 2;
        $canPull = $lsRemoteExitCode === 0;
        if ($output->isVerbose()) {
          $output->writeln($hasRemote ? "Found origin remote, so will pull / push." : "Did not find origin remote.");
        }
      }
      else {
        $hasRemote = $canPull= FALSE;
      }
      if ($canPull) {
        if ($output->isVerbose()) {
          $output->writeln("Pulling...");
        }
        ShellExecutor::succeed("git -C $quotedDirName pull --rebase");
      }

      $this->encryption->write($fileName, $serialized, $recipientFiles);

      if ($hasGit) {
        ShellExecutor::succeed("git -C $quotedDirName add -A");
        ShellExecutor::succeed("git -C $quotedDirName commit -m Autocommit-by-spex");
        if ($output->isVerbose()) {
          $output->writeln("Added git commit.");
        }
      }
      if ($hasRemote) {
        if ($output->isVerbose()) {
          $output->writeln("Pushing...");
        }
        ShellExecutor::succeed("git -C $quotedDirName push origin master");
      }
    }
    return 0;
  }

  private function resdFromFile(string $file): ?string {
    if (is_file($file) && is_readable($file)) {
      return file_get_contents($file);
    }
    return NULL;
  }

}
