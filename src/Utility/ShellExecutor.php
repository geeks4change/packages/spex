<?php

namespace geeks4change\spex\Utility;

class ShellExecutor {

  public static function exec($cmd, &$stdout=null, &$stderr=null): int {
    $process = proc_open($cmd,[
      1 => ['pipe','w'],
      2 => ['pipe','w'],
    ],$pipes);
    $status = proc_get_status($process);
    while ($status["running"]) {
      sleep(1);
      $status = proc_get_status($process);
    }
    $exitcode = $status['exitcode'];
    $stdout = stream_get_contents($pipes[1]);
    fclose($pipes[1]);
    $stderr = stream_get_contents($pipes[2]);
    fclose($pipes[2]);
    proc_close($process);
    return $exitcode;
  }

  public static function succeed($cmd, &$stdout=null, &$stderr=null): void {
    $exitcode = static::exec($cmd, $stdout, $stderr);
    if ($exitcode) {
      throw new \RuntimeException("Got error code $exitcode for '$cmd'\nErr output:\n$stderr");
    }
  }

}
