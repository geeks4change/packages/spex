<?php

namespace geeks4change\spex\Utility;

class RageEncryption {

  public function checkBinary(): void {
    exec('rage --version 2>/dev/null >/dev/null', $_, $rexultCode);
    if ($rexultCode) {
      throw new \RuntimeException('Can not find executable  `rage` in PATH, install it from https://github.com/str4d/rage/releases');
    }
  }

  public function write(string $fileName, string $contents, array $recipientsFiles): void {
    $this->checkBinary();
    $cmd = "rage";
    foreach ($recipientsFiles as $recipientFile) {
      $cmd .= " --recipient " . escapeshellarg($recipientFile);
    }

    $process = proc_open($cmd, [
      0 => ['pipe', 'r'],
      1 => ['file', $fileName, 'w'],
      2 => STDERR,
    ], $pipes);
    $stream = $pipes[0];

    fwrite($stream, $contents);
    fclose($stream);
    $returnValue = proc_close($process);
    if ($returnValue) {
      throw new \RuntimeException("Error writing file $fileName");
    }
  }

  public function read(string $fileName, string $identityFile): string {
    $this->checkBinary();
    $cmd = "rage --decrypt --identity " . escapeshellarg($identityFile);

    $process = proc_open($cmd, [
      0 => ['file', $fileName, 'r'],
      1 => ['pipe', 'w'],
      2 => STDERR,
    ], $pipes);
    $stream = $pipes[1];

    $contents = stream_get_contents($stream);
    fclose($stream);
    $returnValue = proc_close($process);
    if ($returnValue) {
      throw new \RuntimeException("Error reading file $fileName");
    }
    return $contents;
  }

}
