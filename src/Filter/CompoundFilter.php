<?php

namespace geeks4change\spex\Filter;

use geeks4change\spex\View\ViewTimeSpent;

class CompoundFilter implements TimeSpentFilterInterface {

  /**
   * @var \geeks4change\spex\Filter\TimeSpentFilterInterface[]
   */
  protected $filters = [];

  public function addFilter(TimeSpentFilterInterface $filter) {
    $this->filters[] = $filter;
  }


  public function filterTimeSpent(ViewTimeSpent $timeSpent): bool {
    foreach ($this->filters as $filter) {
      if (!$filter->filterTimeSpent($timeSpent)) {
        return FALSE;
      }
    }
    return TRUE;
  }

}
