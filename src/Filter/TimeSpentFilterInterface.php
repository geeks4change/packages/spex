<?php

namespace geeks4change\spex\Filter;

use geeks4change\spex\View\ViewTimeSpent;

interface TimeSpentFilterInterface {

  public function filterTimeSpent(ViewTimeSpent $timeSpent): bool;

}
