<?php

namespace geeks4change\spex\Filter;

use geeks4change\spex\View\ViewTimeSpent;

class TaskPrefixesFilter extends PrefixFilterBase implements TimeSpentFilterInterface {

  protected function getStringToFilter(ViewTimeSpent $timeSpent): string {
    return $timeSpent->getTaskName();
  }

}
