<?php

namespace geeks4change\spex\Filter;

use geeks4change\spex\Aggregation\TimeSpentItem;
use geeks4change\spex\View\ViewTimeSpent;

class WeekFilter implements TimeSpentFilterInterface {

  /**
   * @var string|null
   */
  protected $firstDay;

  /**
   * @var string|null
   */
  protected $lastDay;

  /**
   * WeekFilter constructor.
   * @param string|null $firstDay
   * @param string|null $lastDay
   */
  public function __construct(?string $firstDay, ?string $lastDay) {
    if ($firstDay > $lastDay) {
      throw new \LogicException("First day aftr last day: '$firstDay' > '$lastDay'");
    }
    $this->firstDay = $firstDay;
    $this->lastDay = $lastDay;
  }

  public static function fromSpec(?string $spec) {
    if (is_null($spec)) {
      $firstDay = $lastDay = NULL;
    }
    else {
      if (!$spec) {
        $spec = '+0';
      }
      $pattern = '~^
        ((?<year>\d{4})w)?
        ((?<relweek>[+-])?(?<week>\d\d?))?
        (?<span>[+-](\d\d?))?
      $~x';
      preg_match($pattern, $spec, $match) || static::throwInvalidSpec($spec);
      $year = intval((isset($match['year']) && $match['year'] !== '')
        ? $match['year'] : date('Y'));
      $span = intval($match['span'] ?? 0);
      $weekRaw = $match['week'] ?? '';
      $relWeek = $match['relweek'] ?? NULL;
      $week = $relWeek ? (intval(date('W')) + intval("$relWeek$weekRaw")) : intval($weekRaw);
      $backward = $span < 0;

      if (!$backward) {
        $firstDayOffset = 1;
        $lastDayOffset = 7 + 7 * $span;
      }
      else {
        $lastDayOffset = 7;
        $firstDayOffset = 1 + 7 * $span;
      }

      $firstDay = (new \DateTime())
        ->setISODate($year, $week, $firstDayOffset)
        ->format('Y-m-d');
      $lastDay = (new \DateTime())
        ->setISODate($year, $week, $lastDayOffset)
        ->format('Y-m-d');
    }
    return new static($firstDay, $lastDay);
  }

  protected static function throwInvalidSpec(string $spec) {
    throw new \UnexpectedValueException("Invalid week pattern: $spec");
  }

  public function filterTimeSpent(ViewTimeSpent $timeSpent): bool {
    $day = $timeSpent->getDay();
    return (!$this->firstDay || $day >= $this->firstDay)
      && (!$this->lastDay || $day <= $this->lastDay);
  }

}
