<?php

namespace geeks4change\spex\Filter;

use geeks4change\spex\View\ViewTimeSpent;

class UserFilter implements TimeSpentFilterInterface {

  /**
   * @var string
   */
  protected $userName;

  /**
   * UserFilter constructor.
   * @param string $userName
   */
  public function __construct(string $userName) {
    $this->userName = $userName;
  }


  public function filterTimeSpent(ViewTimeSpent $timeSpent): bool {
    return $timeSpent->getUser() === $this->userName;
  }

}
