<?php

namespace geeks4change\spex\Filter;

use geeks4change\spex\View\ViewTimeSpent;

class PrefixFilterBase {

  /**
   * @var string[]
   */
  protected $prefixes;

  /**
   * ProjectPrefixesFilter constructor.
   * @param string[] $prefixes
   */
  public function __construct(?array $prefixes) {
    $prefixes = $prefixes ?? [];
    $prefixes = array_map('trim', $prefixes);
    $prefixes = array_filter($prefixes);
    $this->prefixes = $prefixes;
  }

  public function filterTimeSpent(ViewTimeSpent $timeSpent): bool {
    $projectName = $this->getStringToFilter($timeSpent);
    if (!$this->prefixes) {
      return TRUE;
    }
    return (bool) array_filter($this->prefixes, function (?string $prefix) use ($projectName) {
      return stripos($projectName, $prefix ?? 'NONE') === 0;
    });
  }

}
